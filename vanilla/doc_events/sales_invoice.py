from frappe.utils import cint
import frappe

from erpnext.accounts.utils import get_account_currency


def customer_balance_gl_entry(doc, *args):
    """
    Makes a debit entry to Debtor account and a credit entry to Cash account.
    It is to be called if a Mode of Payment called 'Customer Balance' is found
    in the invoice. The effect is to adjust the debit posting made by the Sales
    Invoice (Dr Cash, Cr Sales) to reduce Customer's credit balance.

    Specifically, the accounting entries that will be made are:
    Dr Debtor
    Cr Cash

    Hint: This function does the inverse of `make_pos_gl_entries` in
    `sales_invoice.py`
    """
    invoice = doc
    gl_entries = []
    validate(doc)

    for payment in invoice.payments:
        if payment.mode_of_payment == 'Customer Balance' and payment.amount:
            gl_entries.extend(_make_gl_entries(invoice, payment))

    if gl_entries:
        from erpnext.accounts.general_ledger import make_gl_entries
        make_gl_entries(
            gl_entries, cancel=(invoice.docstatus == 2), merge_entries=False,
            update_outstanding='No')


def validate(d):
    """
    Validation checks 1 things:
    1. Check if the total Customer Balance mode of payment is not more than
    what is owed the customer
    """
    validate_total(d)


def validate_total(d):
    if d.is_return:
        return

    total = 0
    for payment in d.payments:
        if payment.mode_of_payment == 'Customer Balance':
            total += payment.amount

    if total > d.customer_balance:
        frappe.throw(
            'Customer balance for {0} must not exceed {1}'.format(
                d.customer, d.customer_balance))


def _make_gl_entries(invoice, payment):
    gl_entries = []
    payment_mode_account_currency = get_account_currency(payment.account)
    debit_in_account_currency = payment.base_amount \
        if invoice.party_account_currency == invoice.company_currency \
        else payment.amount
    against_voucher = invoice.return_against if \
        cint(invoice.is_return) else invoice.name
    credit_in_account_currency = payment.base_amount \
        if payment_mode_account_currency == invoice.company_currency \
        else payment.amount

    gl_entries.append(
        invoice.get_gl_dict({
            "account": invoice.debit_to,
            "party_type": "Customer",
            "party": invoice.customer,
            "against": payment.account,
            "debit": payment.base_amount,
            "debit_in_account_currency": debit_in_account_currency,
            "against_voucher": against_voucher,
            "against_voucher_type": invoice.doctype,
        }, invoice.party_account_currency)
    )

    gl_entries.append(
        invoice.get_gl_dict({
            "account": payment.account,
            "against": invoice.customer,
            "credit": payment.base_amount,
            "credit_in_account_currency": credit_in_account_currency,
        }, payment_mode_account_currency)
    )

    return gl_entries
