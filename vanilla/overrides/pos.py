from erpnext.accounts.doctype.sales_invoice.pos import get_pos_data as _get_pos_data
import frappe


@frappe.whitelist()
def get_pos_data():
    pos_data = _get_pos_data()

    # there is a penalty of 1 extra query and extra calculations but I think
    # its very important that we are connected to upstream as much as
    # possible
    variants_list = get_item_variants(pos_data)
    item_attribute_table = map_variant_to_item(variants_list)

    for item in pos_data['items']:
        if item_attribute_table.get(item.name):
            item['attributes'] = item_attribute_table[item.name]

    return pos_data


def get_item_variants(data):
    item_names = [item.name for item in data['items']]
    variant_list = frappe.db.sql(
        'select name, parent, attribute, attribute_value '
        'FROM `tabItem Variant Attribute` '
        'WHERE parent in %s',
        (item_names,), as_dict=True
    )

    return variant_list


def map_variant_to_item(attribute_list):
    variant_table = {}

    for attribute in attribute_list:
        if variant_table.get(attribute.parent):
            new_value = attribute.attribute_value
            variant_table[attribute.parent].append(new_value)
        else:
            variant_table[attribute.parent] = [attribute.attribute_value]

    return variant_table
